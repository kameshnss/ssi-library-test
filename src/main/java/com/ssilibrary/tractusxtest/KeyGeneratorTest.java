package com.ssilibrary.tractusxtest;

import org.eclipse.tractusx.ssi.lib.crypt.KeyPair;
import org.eclipse.tractusx.ssi.lib.crypt.x21559.x21559Generator;
import org.eclipse.tractusx.ssi.lib.exception.KeyGenerationException;
import org.eclipse.tractusx.ssi.lib.model.base.EncodeType;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class KeyGeneratorTest {

    public static void main(String[] args) {
        x21559Generator keyGenerator = new x21559Generator();
        try {
            KeyPair keyPair = keyGenerator.generateKey();
            String privateKeyString = keyPair.getPrivateKey().asStringForStoring();
            System.out.println("prvK= "+privateKeyString );
            String relativePath = "src/main/resources/keys";
            Path directoryPath = Paths.get(relativePath);
         
            Files.createDirectories(directoryPath);
         
            Path publicKeyPath = directoryPath.resolve("publicKey.pem");
            Path publicKeyEncodedPath = directoryPath.resolve("publicKeyEncoded.txt");
            Path privateKeyPath = directoryPath.resolve("privateKey.pem");
            Path privateKeyEncodedPath = directoryPath.resolve("privateKeyEncoded.txt");
            
            Files.write(publicKeyPath, keyPair.getPublicKey().asStringForStoring().getBytes());
            Files.write(publicKeyEncodedPath, keyPair.getPublicKey().asStringForExchange(EncodeType.Base64).getBytes());

            Files.write(privateKeyPath, keyPair.getPrivateKey().asStringForStoring().getBytes());
            Files.write(privateKeyEncodedPath, keyPair.getPrivateKey().asStringForExchange(EncodeType.Base64).getBytes()); 
            System.out.println("Public Key stored in PEM format at: " + publicKeyPath);
            System.out.println("Public Key stored in encoded form at: " + publicKeyEncodedPath);
            System.out.println("Private Key stored in PEM format at: " + publicKeyPath);
            System.out.println("Private Key stored in encoded form at: " + publicKeyEncodedPath);
            
        } catch (Exception e) {
            System.err.println("An error occurred: " + e.getMessage());
             e.printStackTrace();
        }
    }
}
