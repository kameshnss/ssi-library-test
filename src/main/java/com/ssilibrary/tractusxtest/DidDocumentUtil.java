
package com.ssilibrary.tractusxtest;

import java.net.MulticastSocket;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import org.eclipse.tractusx.ssi.lib.crypt.IKeyGenerator;
import org.eclipse.tractusx.ssi.lib.crypt.IPrivateKey;
import org.eclipse.tractusx.ssi.lib.crypt.IPublicKey;
import org.eclipse.tractusx.ssi.lib.crypt.KeyPair;
import org.eclipse.tractusx.ssi.lib.crypt.jwk.JsonWebKey;
import org.eclipse.tractusx.ssi.lib.crypt.x21559.x21559Generator;
import org.eclipse.tractusx.ssi.lib.crypt.x21559.x21559PrivateKey;
import org.eclipse.tractusx.ssi.lib.crypt.x21559.x21559PublicKey;
import org.eclipse.tractusx.ssi.lib.did.web.DidWebFactory;
import org.eclipse.tractusx.ssi.lib.exception.InvalidePrivateKeyFormat;
import org.eclipse.tractusx.ssi.lib.exception.InvalidePublicKeyFormat;
import org.eclipse.tractusx.ssi.lib.exception.KeyGenerationException;
import org.eclipse.tractusx.ssi.lib.model.MultibaseString;
import org.eclipse.tractusx.ssi.lib.model.base.MultibaseFactory;
import org.eclipse.tractusx.ssi.lib.model.did.Did;
import org.eclipse.tractusx.ssi.lib.model.did.DidDocument;
import org.eclipse.tractusx.ssi.lib.model.did.DidDocumentBuilder;
import org.eclipse.tractusx.ssi.lib.model.did.Ed25519VerificationMethod;
import org.eclipse.tractusx.ssi.lib.model.did.Ed25519VerificationMethodBuilder;
import org.eclipse.tractusx.ssi.lib.model.did.JWKVerificationMethod;
import org.eclipse.tractusx.ssi.lib.model.did.JWKVerificationMethodBuilder;
import org.eclipse.tractusx.ssi.lib.model.did.VerificationMethod;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.OctetKeyPair;
import com.nimbusds.jose.jwk.gen.OctetKeyPairGenerator;

/** This is example class to demonstrate how to create @{@link DidDocument} using Ed25519 key */
public class DidDocumentUtil {

  /**
   * Build did document
   *
   * @param hostName the host name
   * @return the did document
   * @throws KeyGenerationException the key generation exception
   */
 
	public static ObjectNode buildDidDocument1(String hostName) throws Exception {
      
      final Did did = DidWebFactory.fromHostname(hostName);
      final DidDocumentBuilder didDocumentBuilder = new DidDocumentBuilder();
      
      String privateKeyData = new String(Files.readAllBytes(Paths.get("src/main/resources/keys/privateKey.pem")));
      String publicKeyData = new String(Files.readAllBytes(Paths.get("src/main/resources/keys/publicKey.pem")));

      byte[] privateKeyBytes = decodePEM(privateKeyData);
      byte[] publicKeyBytes = decodePEM(publicKeyData);

      
      IPrivateKey privateKey = new x21559PrivateKey(privateKeyBytes);
      IPublicKey publicKey = new x21559PublicKey(publicKeyBytes);

      
      JsonWebKey jwk = new JsonWebKey("1", publicKey, privateKey);

      // Building Verification Methods
      final List<VerificationMethod> verificationMethods = new ArrayList<>();
      final JWKVerificationMethodBuilder builder = new JWKVerificationMethodBuilder();
      final JWKVerificationMethod key = builder.did(did).jwk(jwk).build();
     
      final MultibaseString publicKeyBase = MultibaseFactory.create(publicKey.asByte());
      final Ed25519VerificationMethodBuilder builder1 = new Ed25519VerificationMethodBuilder();
      final Ed25519VerificationMethod key1 =
          builder1
              .id(URI.create(did.toUri() + "#key-" + 1))
              .controller(did.toUri())
              .publicKeyMultiBase(publicKeyBase)
              .build();
      verificationMethods.add(key);
      verificationMethods.add(key1);
    

      didDocumentBuilder.verificationMethods(verificationMethods);
      didDocumentBuilder.id(did.toUri());
      ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.enable(SerializationFeature.INDENT_OUTPUT);  
      JsonNode didNode = objectMapper.valueToTree(didDocumentBuilder.build());
      System.out.println(didNode);
      ObjectNode orderedNode = objectMapper.createObjectNode();
      orderedNode.set("@context", didNode.get("@context"));
      orderedNode.set("id", didNode.get("id"));
     // orderedNode.set("verificationMethod", didNode.get("verificationMethod"));

      // Reorder verificationMethod
      ArrayNode verificationMethod = (ArrayNode) didNode.get("verificationMethod");
      ArrayNode orderedVerificationMethods = objectMapper.createArrayNode();

      for (JsonNode method : verificationMethod) {
          ObjectNode orderedMethod = objectMapper.createObjectNode();
          orderedMethod.set("id", method.get("id"));
          orderedMethod.set("type", method.get("type"));
          orderedMethod.set("controller", method.get("controller"));

          if (method.has("publicKeyJwk")) {
              orderedMethod.set("publicKeyJwk", method.get("publicKeyJwk"));
          }

          if (method.has("publicKeyMultibase")) {
              orderedMethod.set("publicKeyMultibase", method.get("publicKeyMultibase"));
          }

          orderedVerificationMethods.add(orderedMethod);
      }

      orderedNode.set("verificationMethod", orderedVerificationMethods);
      System.out.println(orderedNode);
      
      return orderedNode;
  }

  private static byte[] decodePEM(String pem) {
      String[] tokens = pem.split("\\r?\\n");
      String base64 = String.join("", tokens)
          .replaceFirst("-----BEGIN [^-]+-----", "")
          .replaceFirst("-----END [^-]+-----", "");
      return Base64.getDecoder().decode(base64);
  }
}
