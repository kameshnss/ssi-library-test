package com.ssilibrary.tractusxtest;

import java.io.IOException;
import java.util.Map;

import org.eclipse.tractusx.ssi.lib.exception.InvalidePrivateKeyFormat;
import org.eclipse.tractusx.ssi.lib.exception.KeyGenerationException;
import org.eclipse.tractusx.ssi.lib.exception.SsiException;
import org.eclipse.tractusx.ssi.lib.exception.UnsupportedSignatureTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.springframework.http.ResponseEntity;
@RestController
@RequestMapping("/api")
public class Controller {
    
    @Autowired
    public VCService service;
    
    @Autowired
    public VCjwk vcjwk;


    @GetMapping("/test")
    public String test() {
        return "Test completed";
    }

    @GetMapping("/post")
    public String post() {
        return "Post completed";
    }

    
    @GetMapping("/demoed")
    public JsonNode Demo() throws IOException {
        try {
			return service.Demo();
		} catch (SsiException | InvalidePrivateKeyFormat | KeyGenerationException
				| UnsupportedSignatureTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    @GetMapping("/demojwk")
    public JsonNode Demojwk() throws IOException {
        try {
			return vcjwk.Demojwk();
		} catch (SsiException | InvalidePrivateKeyFormat | KeyGenerationException
				| UnsupportedSignatureTypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    
    
    @PostMapping("/generateDidDocument")
    public ResponseEntity<ObjectNode> generateDidDocument(@RequestBody Map<String, String> requestBody) throws Exception {
        String hostName = requestBody.get("hostName");
        ObjectNode didDocument = DidDocumentUtil.buildDidDocument1(hostName);
        return ResponseEntity.ok(didDocument);
    }
    
    
    
}
