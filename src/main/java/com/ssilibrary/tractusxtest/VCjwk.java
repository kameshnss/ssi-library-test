package com.ssilibrary.tractusxtest;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import org.eclipse.tractusx.ssi.lib.model.proof.ed21559.Ed25519Signature2020;
import org.eclipse.tractusx.ssi.lib.model.proof.jws.JWSSignature2020;
import org.eclipse.tractusx.ssi.lib.model.verifiable.credential.VerifiableCredential;
import org.eclipse.tractusx.ssi.lib.model.verifiable.credential.VerifiableCredentialBuilder;
import org.eclipse.tractusx.ssi.lib.model.verifiable.credential.VerifiableCredentialSubject;
import org.eclipse.tractusx.ssi.lib.model.verifiable.credential.VerifiableCredentialType;
import org.eclipse.tractusx.ssi.lib.proof.LinkedDataProofGenerator;
import org.eclipse.tractusx.ssi.lib.proof.SignatureType;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.eclipse.tractusx.ssi.lib.crypt.IPrivateKey;
import org.eclipse.tractusx.ssi.lib.crypt.KeyPair;
import org.eclipse.tractusx.ssi.lib.crypt.x21559.x21559Generator;
import org.eclipse.tractusx.ssi.lib.crypt.x21559.x21559PrivateKey;
import org.eclipse.tractusx.ssi.lib.exception.InvalidePrivateKeyFormat;
import org.eclipse.tractusx.ssi.lib.exception.KeyGenerationException;
import org.eclipse.tractusx.ssi.lib.exception.SsiException;
import org.eclipse.tractusx.ssi.lib.exception.UnsupportedSignatureTypeException;


@Service
public class VCjwk {

	public JsonNode  Demojwk () throws SsiException, InvalidePrivateKeyFormat, KeyGenerationException, UnsupportedSignatureTypeException, IOException {
		
		  
		  String privateKeyData = new String(Files.readAllBytes(Paths.get("src/main/resources/keys/privateKey.pem")));
		  byte[] privateKeyBytes = decodePEM(privateKeyData);
		  IPrivateKey privateKey = new x21559PrivateKey(privateKeyBytes);
		  

			final VerifiableCredentialBuilder verifiableCredentialBuilder =
			        new VerifiableCredentialBuilder();
			
			final VerifiableCredentialSubject verifiableCredentialSubject =
			        new VerifiableCredentialSubject(Map.of("id", "did:example:ebfeb1f712ebc6f1c276e12ec21",
			        		"value", "Exemple d'Université",
			        		"lang", "fr"));
			
			  final VerifiableCredential credentialWithProof =
				        verifiableCredentialBuilder
				            .expirationDate(Instant.now().plusSeconds(3600))
				            .credentialSubject(verifiableCredentialSubject)
				            .issuanceDate(Instant.now())
				            .issuer(URI.create("did:test:isser"))
				            .type(List.of(VerifiableCredentialType.VERIFIABLE_CREDENTIAL))
				            .id(URI.create("did:test:id"))
				            .build();
			  

				    // JWS Proof Builder
				    final LinkedDataProofGenerator generator =
				        LinkedDataProofGenerator.newInstance(SignatureType.JWS);
				    final JWSSignature2020 proof =
				        (JWSSignature2020)
				            generator.createProof(credentialWithProof, URI.create("did:test:isser" + "#key-1"), privateKey);
			    
			    verifiableCredentialBuilder.proof(proof);
			    ObjectMapper objectMapper = new ObjectMapper();
		        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);  

		        // Convert to JSON Node
		        JsonNode credentialNode = objectMapper.valueToTree(verifiableCredentialBuilder.build());

		        // Create a new ObjectNode to hold the fields in the desired order
		        ObjectNode orderedNode = objectMapper.createObjectNode();

		        // Add fields in the desired order
		        orderedNode.set("@context", credentialNode.get("@context"));
		        orderedNode.set("id", credentialNode.get("id"));
		        orderedNode.set("type", credentialNode.get("type"));
		        orderedNode.set("issuer", credentialNode.get("issuer"));
		        orderedNode.set("issuanceDate", credentialNode.get("issuanceDate"));
		        orderedNode.set("credentialSubject", credentialNode.get("credentialSubject"));
		        orderedNode.set("proof", credentialNode.get("proof"));

		        // Convert orderedNode to JSON string
		        String json = objectMapper.writeValueAsString(orderedNode);
		        System.out.println(json);
		       // return verifiableCredentialBuilder.build();
		        //String json = objectMapper.writeValueAsString(credentialWithProof);
		        
		        return orderedNode;
		  
	}
	

	
	 public IPrivateKey getKey(String filePath) throws IOException, InvalidePrivateKeyFormat {
	       
	        String keyContent = new String(Files.readAllBytes(Paths.get(filePath)));
	        
	        return new x21559PrivateKey(keyContent, true);
	    }
	 private static byte[] decodePEM(String pem) {
	      String[] tokens = pem.split("\\r?\\n");
	      String base64 = String.join("", tokens)
	          .replaceFirst("-----BEGIN [^-]+-----", "")
	          .replaceFirst("-----END [^-]+-----", "");
	      return Base64.getDecoder().decode(base64);
	  }

}
