package com.ssilibrary.tractusxtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsilibraryTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsilibraryTestApplication.class, args);
	}

}
